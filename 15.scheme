					; Advent of code, Day 15

(define gen-a-value 0)

(define (reset-a value)
  (set! gen-a-value value)
  gen-a-value
  )

(define (gen-a)
  (set! gen-a-value (modulo (* gen-a-value 16807) 2147483647))
  gen-a-value
  )

(define gen-b-value 0)

(define (reset-b value)
  (set! gen-b-value value)
  gen-b-value
  )

(define (gen-b)
  (set! gen-b-value (modulo (* gen-b-value 48271) 2147483647))
  gen-b-value
  )

(define (gen-a-picky)
  (if (zero? (modulo (gen-a) 4))
      gen-a-value
      (gen-a-picky)
      )
  )

(define (gen-b-picky)
  (if (zero? (modulo (gen-b) 8))
      gen-b-value
      (gen-b-picky)
      )
  )

(define (match-16 a b)
  (equal? (modulo a 65536) (modulo b 65536))
  )

(define (matches a b reps)
  (define (_m reps count)
    (if (zero? reps) count
	(begin
	  (if (match-16 (gen-a) (gen-b))
	      (set! count (+ 1 count))
	      )
	  (_m (- reps 1) count)
	  )
	)
    )
  (reset-a a)
  (reset-b b)
  (_m reps 0)
  )

(define (matches-picky a b reps)
  (define (_m reps count)
    (if (zero? reps) count
	(begin
	  (if (match-16 (gen-a-picky) (gen-b-picky))
	      (set! count (+ 1 count))
	      )
	  (_m (- reps 1) count)
	  )
	)
    )
  (reset-a a)
  (reset-b b)
  (_m reps 0)
  )

(define (run-match args)
  (apply matches args)
  )

(define (run-match-picky args)
  (apply matches-picky args)
  )

(define (check want fun arg)
  (let ((got (fun arg)))
    (if (equal? want got)
	(begin
	  (display "Test success.") (newline)
	  )
	(begin
	  (display "Test failure. Wanted ") (write want) (display " got ") (write got) (newline)
	  (exit)
	  )
	)
    )
  )

(define (run label fun arg)
  (let ((got (fun arg)))
    (display "Result ") (display label) (display " ") (write got) (newline)
    )
  )

(define test-input-1 '(65 8921 5))
(define test-input-2 '(65 8921 40000000))
(define test-input-3 '(65 8921 5000000))

(check 1 run-match test-input-1)
;(check 588 run-match test-input-2)
(check 0 run-match-picky test-input-1)
(check 309 run-match-picky test-input-3)

(define input-1 '(591 393 40000000))
(define input-2 '(591 393 5000000))

(run "part 1" run-match input-1)
(run "part 2" run-match-picky input-2)
